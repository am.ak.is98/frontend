const { colors } = require('tailwindcss/defaultTheme');
const fs = require('fs');
const { buildSchema } = require('graphql');
const fetch = require('node-fetch');
const util = require('util');

const readFileAsync = util.promisify(fs.readFile);
const setTimeoutAsync = time => new Promise(resolve => setTimeout(resolve, time));

module.exports = {
	siteMetadata: {
		siteUrl: 'https://sscsalex.org',
		title: `IEEE SSCS AlexSC`,
		description: `IEEE SSCS Alexandria Student Chapter`
	},
	plugins: [
		`gatsby-plugin-react-helmet`,
		{
			resolve: `gatsby-plugin-manifest`,
			options: {
				name: `IEEE SSCS AlexSC`,
				short_name: `SSCS AlexSC`,
				start_url: `/`,
				background_color: colors.red[100],
				theme_color: colors.red[200],
				display: `minimal-ui`,
				icon: `src/images/favicon.png`
			}
		},
		`gatsby-plugin-postcss`,
		'gatsby-transformer-sharp',
		'gatsby-plugin-sharp',
		{
			resolve: 'gatsby-source-filesystem',
			options: {
				name: 'images',
				path: `${__dirname}/src/images`
			}
		},
		{
			resolve: 'gatsby-source-graphql',
			options: {
				// Arbitrary name for the remote schema Query type
				typeName: 'CMS',
				// Field under which the remote schema will be accessible. You'll use this in your Gatsby query
				fieldName: 'cms',
				// Url to query from
				url:
					process.env.NODE_ENV === 'production'
						? 'https://admin.sscsalex.org/admin/api/'
						: 'http://localhost:3000/admin/api/',
				createSchema: async () => buildSchema((await readFileAsync(`${__dirname}/schema.graphql`)).toString()),
				batch: true,
				// Workaround until https://github.com/keystonejs/keystone/issues/2709 is fixed.
				headers: { Accept: 'application/json' },
				fetch: async (uri, options = {}) => {
					let response = await fetch(uri, options);
					const json = response.json();
					response.json = async () => {
						let res = await json;
						while (res.loading) {
							await setTimeoutAsync(500);
							res = await (await fetch(uri, options)).json();
						}
						return res;
					};
					return response;
				}
			}
		},
		`gatsby-plugin-offline`,
		{
			resolve: `gatsby-plugin-google-gtag`,
			options: {
				trackingIds: ['G-21FRD58F44']
			}
		},
		'gatsby-plugin-sri',
		{
			resolve: 'gatsby-plugin-sitemap',
			options: {
				query: `{
					site {
						siteMetadata {
							siteUrl
						}
					}
					allSitePage {
						nodes {
							path
						}
					}
				}`,
				serialize: ({ site, allSitePage }) =>
					allSitePage.nodes.map(page => ({
						url: `${site.siteMetadata.siteUrl}${page.path}`,
						changefreq: 'weekly',
						priority: page.path === '/' ? 1 : 0.7
					}))
			}
		}
	]
};
