const path = require('path');
const { createRemoteFileNode } = require('gatsby-source-filesystem');

exports.createResolvers = ({ actions: { createNode }, cache, createNodeId, createResolvers, store, reporter }) =>
	createResolvers({
		CMS_File: {
			gatsbyFile: {
				type: 'File',
				resolve: source =>
					createRemoteFileNode({
						url:
							process.env.NODE_ENV === 'production'
								? source.publicUrl
								: `http://localhost:3000${source.publicUrl}`,
						store,
						cache,
						createNode,
						createNodeId,
						reporter
					})
			}
		}
	});

exports.createPages = async ({ actions, graphql }) => {
	const result = await graphql(`
		{
			cms {
				allCourses {
					id
				}
				allArticles {
					id
				}
				allBios {
					id
				}
			}
		}
	`);

	if (result.errors) throw result.errors;

	for (const { id } of result.data.cms.allCourses) {
		actions.createPage({
			path: `/courses/${id}`,
			component: path.resolve(`src/templates/course.jsx`),
			context: { id }
		});
	}
	for (const { id } of result.data.cms.allArticles) {
		actions.createPage({
			path: `/articles/${id}`,
			component: path.resolve(`src/templates/article.jsx`),
			context: { id }
		});
	}
	for (const { id } of result.data.cms.allBios) {
		actions.createPage({
			path: `/bio/${id}`,
			component: path.resolve(`src/templates/bio.jsx`),
			context: { id }
		});
	}
};
